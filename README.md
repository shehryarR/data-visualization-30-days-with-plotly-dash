# Data Visualization - 30 days with Plotly Dash


## Description
This project is intended to help the newbies learn Plotly Dash with code examples using real world datasets. Write Ups are available on Medium

## Visuals


## Pre-requisite
Must have Python installed along with plotly dash. You can find adequate resource for the commands to use at [this link](https://dash.plotly.com/installation). Its is better to install the following packages as well:
- numpy
- seaborn
- dash_bootstrap_components
each of these can be installed using pip as
`pip install numpy`


## Usage
Run the _.py_ file from the command line as `python dashboard.py` and then view at _127.0.0.1:8050_ 


## Want me to work on your dashboard?
Contact me [here](https://www.fiverr.com/msraza01/create-optimize-and-fix-any-plotly-dash-dashboard-in-python)

## Contributing
You can suggest any dataset and/or problem that you face and we would love to incorporate that in any upcoming demo.


## License
Creative Commons (CC)

